from PIL import Image
from utils import should_consider_image
import numpy
import matplotlib.pyplot as plt
import pandas as pd


def process_reference_image(in_file_name, out_file_name):
    # Open image
    img = Image.open('input/' + in_file_name)

    # Split image into channels
    img_r, img_g, img_b, img_a = img.split()

    # Covert channels to arrays
    img_arr_r = numpy.asarray(img_r).astype('float32')
    img_arr_g = numpy.asarray(img_g).astype('float32')
    img_arr_b = numpy.asarray(img_b).astype('float32')

    # Read pixel density values
    pixel_occurrences = {}

    for x in range(0, 256):
        pixel_occurrences[str(x)] = 0

    for x in range(0, len(img_arr_r)):
        for y in range(0, len(img_arr_r[x])):
            if should_consider_image(img_arr_r[x][y], img_arr_g[x][y], img_arr_b[x][y]):
                pixel_density = str(int(img_arr_r[x][y]))
                pixel_occurrences[pixel_density] = pixel_occurrences[pixel_density] + 1

    # Write to CSV file
    df = pd.DataFrame(
        {'Pixel density': list(pixel_occurrences.keys()), 'Occurrences': list(pixel_occurrences.values())},
        columns=['Pixel density', 'Occurrences'])
    df.to_csv(r'output/' + out_file_name + '.csv', index=False, header=True, sep=';')

    # Create chart
    fig, ax = plt.subplots(figsize=(15, 3))
    ax.bar(pixel_occurrences.keys(), pixel_occurrences.values())
    fig.suptitle('Pixel occurrences')
    plt.savefig('output/' + out_file_name + '.svg')


if __name__ == '__main__':
    process_reference_image('china-border-2000.png', 'china-border-2000')
    print('Finished first')
    process_reference_image('china-border-2020.png', 'china-border-2020')
    print('Finished second')
