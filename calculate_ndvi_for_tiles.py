from PIL import Image
import numpy
import matplotlib
import matplotlib.pyplot as plt
from GPSPhoto import gpsphoto
from utils import create_empty_array, is_pixel_white, is_pixel_grey
from shutil import rmtree
from os import listdir, makedirs
from os.path import isfile, join, exists
from palettable.colorbrewer.diverging import RdYlGn_11


def calculate_ndvi(in_file, out_file, dpi=600):
    # Open image to be converted
    img = Image.open(in_file)

    # Split image channels, ignore alpha if present
    img_r, img_g, img_b = img.split()[0:3]

    # Place Red and Blue values in numpy arrays
    img_arr_r = numpy.asarray(img_r).astype('float32')
    img_arr_g = numpy.asarray(img_g).astype('float32')
    img_arr_b = numpy.asarray(img_b).astype('float32')

    # Calculate NDVI
    ndvi_img_data = create_empty_array(img_arr_r)

    is_empty = True

    for x in range(0, len(img_arr_r)):
        for y in range(0, len(img_arr_r[x])):
            if not is_pixel_grey(img_arr_r[x][y], img_arr_g[x][y], img_arr_b[x][y]):
                is_empty = False
                r = img_arr_r[x][y] * img_arr_r[x][y] * img_arr_r[x][y] * img_arr_r[x][y] * img_arr_r[x][y] * img_arr_r[x][y]
                b = img_arr_b[x][y] * img_arr_b[x][y] * img_arr_b[x][y] * img_arr_b[x][y] * img_arr_b[x][y] * img_arr_b[x][y]
                r_minus_b = r - b
                r_plus_b = r + b
                if not r_plus_b == 0:
                    ndvi_img_data[x][y] = r_minus_b / r_plus_b + 0.2
                else:
                    ndvi_img_data[x][y] = 0.2
            else:
                ndvi_img_data[x][y] = 2

    if not is_empty:
        
        # Create jet colour map
        #custom_cmap = plt.set_cmap('jet')
        img_w, img_h = img.size

        # Lay out the plot, making room for a color scale bar
        fig_w = img_w/dpi
        fig_h = img_h/dpi
        fig = plt.figure(figsize=(fig_w, fig_h), dpi=dpi)
        fig.set_frameon(False)

        # Make an axis for the image filling the whole figure except colorbar space
        ax_rect = [
            0.0,  # left
            0.0,  # bottom
            1.0,  # width
            1.0]  # height

        ax = fig.add_axes(ax_rect)
        ax.yaxis.set_ticklabels([])
        ax.xaxis.set_ticklabels([])
        ax.set_axis_off()
        ax.patch.set_alpha(0.0)

        # Create plot
        axes_img = ax.imshow(ndvi_img_data,
                                #cmap=custom_cmap,
                                cmap=RdYlGn_11.mpl_colormap,
                                vmin=-1,
                                vmax=1,
                                #aspect='equal',
                                )

        # Write new image file
        fig.savefig(out_file, dpi=dpi)

        plt.close()

        img_NDVI = Image.open(out_file)
        # Split image channels, ignore alpha if present
        img_r_NDVI, img_g_NDVI, img_b_NDVI = img_NDVI.split()[0:3]

        # Place Red and Blue values in numpy arrays
        img_arr_r_NDVI = numpy.asarray(img_r_NDVI).astype('float32')
        img_arr_g_NDVI = numpy.asarray(img_g_NDVI).astype('float32')
        img_arr_b_NDVI = numpy.asarray(img_b_NDVI).astype('float32')

        data = numpy.zeros((256, 256, 3), dtype=numpy.uint8)

        for x in range(0, len(ndvi_img_data)):
            for y in range(0, len(ndvi_img_data[x])):
                if ndvi_img_data[x][y] == 2:
                    data[x][y] = [221,221,221]
                else:
                    data[x][y][0] = img_arr_r_NDVI[x][y]
                    data[x][y][1] = img_arr_g_NDVI[x][y]
                    data[x][y][2] = img_arr_b_NDVI[x][y]

        img_NDVI = Image.fromarray(data, 'RGB')
        img_NDVI.save(out_file)
        img_NDVI.close()
        data = None


    img.close()

    ndvi_img_data = None


if __name__ == '__main__':
    if exists('output/small-zoom-ndvi'):
        rmtree('output/small-zoom-ndvi')
    makedirs('output/small-zoom-ndvi')
    files = [f for f in listdir('input/small-zoom/') if isfile(join('input/small-zoom', f))]
    files.sort()
    for file in files:
        calculate_ndvi('input/small-zoom/' + file, 'output/map-3-ndvi/' + file)
        print('Finished: ' + file + ' ' + str(files.index(file)) + '/' + str(len(files)))