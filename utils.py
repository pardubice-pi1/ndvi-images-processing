import numpy as np


def count_ndvi_from_rgb(r, g, b):
    if r == g and g == b:
        return r / 127.5 - 1
    return 0


def is_equal_google_maps_water(r, g, b):
    return r == 167 and g == 217 and b == 253


def is_rgb_equal(r, g, b):
    return r == g and g == b


def should_consider_pixel(r, g, b):
    return not is_equal_google_maps_water(r, g, b) and is_rgb_equal(r, g, b)


def create_empty_array(img_arr):
    return np.zeros((len(img_arr), len(img_arr[0])), dtype=np.float)


def is_pixel_white(r, g, b):
    return r == 255 and g == 255 and b == 255

def is_pixel_grey(r, g, b):
    return r == 221 and g == 221 and b == 221