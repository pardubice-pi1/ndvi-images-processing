from PIL import Image
from utils import count_ndvi_from_rgb, should_consider_pixel, create_empty_array
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def visualise_img(file_name_path, ndvi_data, img_w, img_h, text, dpi=600):
    fig_w = img_w / dpi
    fig_h = img_h / dpi
    fig = plt.figure(figsize=(fig_w, fig_h), dpi=dpi)
    fig.set_frameon(False)

    # Make an axis for the image filling the whole figure except colorbar space
    ax_rect = [
        0.0,  # left
        0.0,  # bottom
        1.0,  # width
        1.0]  # height

    ax = fig.add_axes(ax_rect)
    ax.patch.set_alpha(0.0)

    ndvi_max = np.amax(ndvi_data)
    ndvi_min = np.amin(ndvi_data)

    # Create plot
    custom_cmap = plt.set_cmap('RdYlGn')
    axes_img = ax.imshow(ndvi_data,
                         cmap=custom_cmap,
                         vmin=-1,
                         vmax=1,
                         aspect='equal'
                         )
    # ax.text(0.9, 0.9, text,
    #        verticalalignment='bottom', horizontalalignment='right',
    #        transform=ax.transAxes,
    #        color='black', fontsize=3)

    # Write to file
    # cax = fig.add_axes([0.95, 0.05, 0.025, 0.90])
    # cbar = fig.colorbar(axes_img, cax=cax)
    fig.savefig(file_name_path, dpi=dpi)
    plt.close(fig)


def calculate_ndvi_diff(first_in_file_name, second_in_file_name, out1_file_name, out2_file_name, diff_name,
                        csv_file_name=None):
    # Open images
    first_img = Image.open('input/' + first_in_file_name)
    second_img = Image.open('input/' + second_in_file_name)

    # Split images into channels
    first_img_r, first_img_g, first_img_b, first_img_a = first_img.split()
    second_img_r, second_img_g, second_img_b, second_img_a = second_img.split()

    # Covert channels to arrays
    img1_arr_r = np.asarray(first_img_r).astype('float32')
    img1_arr_g = np.asarray(first_img_g).astype('float32')
    img1_arr_b = np.asarray(first_img_b).astype('float32')
    img2_arr_r = np.asarray(second_img_r).astype('float32')
    img2_arr_g = np.asarray(second_img_g).astype('float32')
    img2_arr_b = np.asarray(second_img_b).astype('float32')

    # Read pixel mappings
    pixel_mapping = {}

    # Convert pixels to NDVI
    ndvi_img1_data_origo = create_empty_array(img1_arr_r)
    ndvi_img2_data_origo = create_empty_array(img2_arr_r)
    ndvi_img2_data_mapped = create_empty_array(img2_arr_r)
    ndvi_result_data_origo = create_empty_array(img1_arr_r)
    ndvi_result_data_mapped = create_empty_array(img2_arr_r)

    if csv_file_name is not None:
        data = pd.read_csv('input/' + csv_file_name, sep=';')

        for x in range(0, len(data.values)):
            pixel_mapping[data.values[x][0]] = data.values[x][1]

    for x in range(0, len(img1_arr_r)):
        for y in range(0, len(img1_arr_r[x])):
            consider_img1 = should_consider_pixel(img1_arr_r[x][y], img1_arr_g[x][y], img1_arr_b[x][y])
            consider_img2 = should_consider_pixel(img2_arr_r[x][y], img2_arr_g[x][y], img2_arr_b[x][y])
            if consider_img1 and consider_img2:
                pixel_density1 = int(img1_arr_r[x][y])
                pixel_density2 = int(img2_arr_r[x][y])

                ndvi1_origo = count_ndvi_from_rgb(pixel_density1, pixel_density1, pixel_density1)
                ndvi2_origo = count_ndvi_from_rgb(pixel_density2, pixel_density2, pixel_density2)

                ndvi_img1_data_origo[x][y] = ndvi1_origo
                ndvi_img2_data_origo[x][y] = ndvi2_origo

                ndvi_result_data_origo[x][y] = (ndvi2_origo + 1) - (ndvi1_origo + 1)

                if csv_file_name is not None:
                    mapped_pixel_density2 = pixel_mapping[pixel_density2]
                    ndvi2_mapped = count_ndvi_from_rgb(mapped_pixel_density2, mapped_pixel_density2,
                                                       mapped_pixel_density2)

                    ndvi_img2_data_mapped[x][y] = ndvi2_mapped
                    ndvi_result_data_mapped[x][y] = (ndvi2_mapped + 1) - (ndvi1_origo + 1)

    img_w, img_h = first_img.size
    visualise_img('output/' + out1_file_name + '-origo.png', ndvi_img1_data_origo, img_w, img_h, '1 origo')
    visualise_img('output/' + out2_file_name + '-origo.png', ndvi_img2_data_origo, img_w, img_h, '2 origo')

    visualise_img('output/' + diff_name + '-origo.png', ndvi_result_data_origo, img_w, img_h, 'diff origo')
    if csv_file_name is not None:
        visualise_img('output/' + out2_file_name + '-mapped.png', ndvi_img2_data_mapped, img_w, img_h, 'map')
        visualise_img('output/' + diff_name + '-mapped.png', ndvi_result_data_mapped, img_w, img_h, 'diff map')


if __name__ == '__main__':
    calculate_ndvi_diff('china-border-2000.png', 'china-border-2020.png', 'china-border-2000-2', 'china-border-2020-2',
                        'china-border-diff-2', csv_file_name='china-border-map.csv')
    print('Finished')
