from PIL import Image
import numpy
import matplotlib
import matplotlib.pyplot as plt
from GPSPhoto import gpsphoto
from utils import create_empty_array, is_pixel_white, is_pixel_grey
from shutil import rmtree
from os import listdir, makedirs
from os.path import isfile, join, exists
from palettable.colorbrewer.diverging import RdYlGn_11
import shutil


def copy_file_non_empty(in_file, out_file):
    # Open image to be converted
    img = Image.open(in_file)

    # Split image channels, ignore alpha if present
    img_r, img_g, img_b = img.split()[0:3]

    # Place Red and Blue values in numpy arrays
    img_arr_r = numpy.asarray(img_r).astype('float32')
    img_arr_g = numpy.asarray(img_g).astype('float32')
    img_arr_b = numpy.asarray(img_b).astype('float32')

    for x in range(0, len(img_arr_r)):
        for y in range(0, len(img_arr_r[x])):
            if not is_pixel_grey(img_arr_r[x][y], img_arr_g[x][y], img_arr_b[x][y]):
                shutil.copy(in_file, out_file)
                img.close()
                return
                
    img.close()


if __name__ == '__main__':
    if exists('output/map-1-copied'):
        rmtree('output/map-1-copied')
    makedirs('output/map-1-copied')
    files = [f for f in listdir('output/map-1/') if isfile(join('output/map-1', f))]
    files.sort()
    for file in files:
        copy_file_non_empty('output/map-1/' + file, 'output/map-1-copied/' + file)
        print('Finished: ' + file + ' ' + str(files.index(file)) + '/' + str(len(files)))